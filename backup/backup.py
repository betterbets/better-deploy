from datetime import datetime
import os
from subprocess import call

import boto3


BACKUP_TIMESTAMP = datetime.now().strftime('%Y%m%d_%H%M%S')
FILENAME = 'dump_{}.sql'.format(
    BACKUP_TIMESTAMP
)

BUCKET_NAME = 'betterbets-dashboard-backups'  # As per bucket name in Terraform
ENVIRONMENT = os.environ.get('ENVIRONMENT')  # stag or prod
POSTGRES_HOST = 'db'
POSTGRES_DB = os.environ.get('POSTGRES_DB')
POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
PG_PASS_PATH = '/root/.pgpass'


def ensure_pgpass_exists(func):
    """Create pgpass file for pg_restore.

    pg_restore cannot have password passed as an argument and requires a
    .pgpass file to be present in the home directory of the user running the
    command.
    """
    def wrapper():
        if not os.path.isfile(PG_PASS_PATH):
            with open('/root/.pgpass', 'w') as pass_file:
                # hostname:port:database:username:password
                pass_file.write('{}:{}:{}:{}:{}'.format(
                    POSTGRES_HOST,
                    5432,
                    POSTGRES_DB,
                    POSTGRES_USER,
                    POSTGRES_PASSWORD
                ))
            os.chmod(PG_PASS_PATH, 0o600)
        func()
    return wrapper


@ensure_pgpass_exists
def backup():
    """Backup Database from the "db" container."""
    with open(FILENAME, 'w') as backup_file:
        call(
            ['pg_dump', '-c', '-h', POSTGRES_HOST, '-U', POSTGRES_USER, POSTGRES_DB],
            stdout=backup_file
        )
    call(['bzip2', '-z', '{}'.format(FILENAME)])

    file_key = '{}/{}/{}/{}.bz2'.format(
        ENVIRONMENT,
        datetime.strptime(BACKUP_TIMESTAMP, '%Y%m%d_%H%M%S').year,
        datetime.strptime(BACKUP_TIMESTAMP, '%Y%m%d_%H%M%S').month,
        FILENAME
    )
    s3 = boto3.client('s3')
    # Organized backups for rollbacks
    s3.upload_file('{}.bz2'.format(FILENAME), BUCKET_NAME, file_key)
    if ENVIRONMENT == 'prod':
        # Convenient backup of the latest database to be easily used by builds
        s3.upload_file(
            '{}.bz2'.format(FILENAME),
            BUCKET_NAME,
            'latest.sql.bz2'.format(ENVIRONMENT)
        )


if __name__ == '__main__':
    backup()
