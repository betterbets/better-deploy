# Better Deploy
Betterbets dashboard deployment files - all the required configs and scripts to run the betterbets dashboard in
staging/production.


## Running Docker Compose
The app is currently using Docker Compose to run all the required containers on the 'staging' and 'production'
severs. These sit behind a load balancer where the TLS is terminated.

In order to use this repository, you must complete the following steps

1.  Copy the `env.example` to `.env` file and set the variables according to the environment.
2.  Run the following command to start the containers.

    ```bash
    docker-compose up -d
    ```
    
    This will run the Dashboard, Postgres, Redis, Celery and Nginx containers using the environment variables present
    in the .env file.


## Backup Postgres Database
In order to backup the betterbets postgres database, use the following commands:

1.  Building the image

    ```bash
    docker build -t registry.gitlab.com/betterbets/better-deploy/better-backup backup
    ```
 
    After logging in to `registry.gitlab.com`, you can use the following command to pull the latest image from GitLab.
   
    ```bash
    docker pull registry.gitlab.com/betterbets/better-deploy/better-backup 
    ```

2.  Run the betterbets-backup container

    ```bash
    docker run -it --rm --network deploy_default --env-file .env registry.gitlab.com/betterbets/better-deploy/better-backup
    ```

    __Note:__ The network name depends on the name of the directory `docker-compose` was run from. On the production
    and staging machines, the directory is called `deploy`.
